<?php


namespace App\Logic;


use App\Interfaces\ProjectDateInterface;

class ProjectDateCalculator implements ProjectDateInterface
{
    /**
     * @param \DateTime $date
     * @return string
     */
    public function crateDateFormat(\DateTime $date): string{
        return date_format($date, 'Y-m-d H:i:s');
    }

    /**
     * @param string $date
     * @return int
     */
    public function getHours(string $date): int{
        return date('H', strtotime($date));
    }

    /**
     * @param string $date
     * @return int
     */
    public function getMinute(string $date): int{
        return date('i', strtotime($date));
    }

    /**
     * @param int $hours
     * @param \DateTime $date
     * @return \DateTime
     */
    public function addHours(int $hours, \DateTime $date): \DateTime{
        return date_add($date, date_interval_create_from_date_string($hours.' hours'));
    }

    /**
     * @param int $days
     * @param \DateTime $date
     * @return \DateTime
     */
    public function addDays(int $days, \DateTime $date): \DateTime{
        return date_add($date, date_interval_create_from_date_string($days.' days'));
    }

    /**
     * @param string $date
     * @return int
     */
    public function getDayOfWeek(string $date): int{
        return date('N', strtotime($date));
    }
}