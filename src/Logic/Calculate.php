<?php


namespace App\Logic;


use App\Structure\ProjectDate;

class Calculate
{
    const WORK_TIME_START = 9;

    const WORK_TIME_STOP = 17;

    const WORKING_TIME = 8;

    const WEEKEND_DAY = 6;

    /**
     * @var ProjectDateCalculator
     */
    private $projectDateCalculator;

    /**
     * Calculate constructor.
     * @param ProjectDateCalculator $projectDateCalculator
     */
    public function __construct(ProjectDateCalculator $projectDateCalculator)
    {
        $this->projectDateCalculator = $projectDateCalculator;
    }

    /**
     * @param ProjectDate $projectDate
     * @return String
     */
    public function calculateDueDate(ProjectDate $projectDate): String {

        $dueDate = 'Today is weekend!';

        if($this->isInWorkTime($projectDate->getStartDate())){

            $date = date_create($projectDate->getStartDate());
            $plusDay = 0;
            $currentHour = $this->projectDateCalculator->getHours($projectDate->getStartDate());

            $dewelopmentDays = $this->getDays($projectDate->getDevelopmentTime());
            $dewelopmentHours = $this->getHours($projectDate->getDevelopmentTime());
            $plusHours = $dewelopmentHours;

            list($plusDay, $plusHours) = $this->checkIsOverTheWorkingTime($projectDate, $currentHour,
                $dewelopmentHours, $plusDay, $date, $plusHours);

            for($i = 0;$i < ($dewelopmentDays+$plusDay);$i++){

                $date = $this->projectDateCalculator->addDays(1, $date);
                $date = date_create($this->skipTheWeekend($this->projectDateCalculator->crateDateFormat($date)));

            }

            $date = $this->projectDateCalculator->addHours($plusHours, $date);
            $dueDate = $this->projectDateCalculator->crateDateFormat($date);

            $dueDate = $this->skipTheWeekend($dueDate);
        }

        return $dueDate;
    }

    /**
     * @param string $startDate
     * @return bool
     */
    public function isWeekend(string $startDate): bool {

        return ($this->projectDateCalculator->getDayOfWeek($startDate) >= self::WEEKEND_DAY);
    }

    /**
     * @param $startDate
     * @return bool
     */
    public function isInWorkTime($startDate): bool {
        return ($this->projectDateCalculator->getHours($startDate) >= self::WORK_TIME_START &&
            $this->projectDateCalculator->getHours($startDate) < self::WORK_TIME_STOP && !$this->isWeekend($startDate));
    }

    /**
     * @param $developTime
     * @return string
     */
    public function getLeftDayAndHours($developTime): string{
        return $this->getDays($developTime) . ' day(s) and ' . $this->getHours($developTime) . ' hour(s)';
    }

    /**
     * @param $developTime
     * @return int
     */
    public function getDays($developTime): int {
        return intval($developTime / self::WORKING_TIME );
    }

    /**
     * @param $developTime
     * @return int
     */
    public function getHours($developTime) :int {
         return ($developTime % self::WORKING_TIME);
    }

    /**
     * @param string $dueDate
     * @return string
     */
    public function skipTheWeekend(string $dueDate):string{
        if(!$this->isInWorkTime($dueDate)){
            $dayOfWeek = $this->projectDateCalculator->getDayOfWeek($dueDate);
            $date = date_create($dueDate);
            if($dayOfWeek >= self::WEEKEND_DAY ){
                $date = $this->projectDateCalculator->addDays(2, $date);
            }

            $dueDate = $this->projectDateCalculator->crateDateFormat($date);
        }

        return $dueDate;
    }

    /**
     * @param ProjectDate $projectDate
     * @param int $currentHour
     * @param int $dewelopmentHours
     * @param int $plusDay
     * @param \DateTime $date
     * @param int $plusHours
     * @return array
     */
    public function checkIsOverTheWorkingTime(ProjectDate $projectDate, int $currentHour,
                                              int $dewelopmentHours, int $plusDay, \DateTime $date, int $plusHours): array
    {
        if ($currentHour + $dewelopmentHours >= self::WORK_TIME_STOP) {
            $plusDay++;

            $date->setTime(self::WORK_TIME_START,
                $this->projectDateCalculator->getMinute($projectDate->getStartDate()));

            $plusHours = ($currentHour + $dewelopmentHours) - self::WORK_TIME_STOP;
        }
        return [$plusDay, $plusHours];
    }

}