<?php


namespace App\Structure;


class ProjectDate
{

    /**
     * @var string
     */
    private $startDate;

    /**
     * @var int
     */
    private $developmentTime;

   public static function crateProjectDate(string $startDate, int $developmentTime): self{
        return new self($startDate, $developmentTime);
   }

   public function __construct(string $startDate, int $developmentTime)
   {
       $this->startDate = $startDate;
       $this->developmentTime = $developmentTime;
   }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * @return int
     */
    public function getDevelopmentTime(): int
    {
        return $this->developmentTime;
    }

}