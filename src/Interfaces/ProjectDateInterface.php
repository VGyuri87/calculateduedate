<?php


namespace App\Interfaces;


interface ProjectDateInterface
{
    /**
     * @param \DateTime $date
     * @return string
     */
    public function crateDateFormat(\DateTime $date):string;

    /**
     * @param string $date
     * @return int
     */
    public function getHours(string $date): int;

    /**
     * @param string $date
     * @return int
     */
    public function getMinute(string $date): int;

    /**
     * @param int $hours
     * @param \DateTime $date
     * @return \DateTime
     */
    public function addHours(int $hours, \DateTime $date): \DateTime;

    /**
     * @param int $days
     * @param \DateTime $date
     * @return \DateTime
     */
    public function addDays(int $days, \DateTime $date): \DateTime;

    /**
     * @param string $date
     * @return int
     */
    public function getDayOfWeek(string $date): int;
}