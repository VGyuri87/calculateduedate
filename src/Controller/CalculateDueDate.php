<?php


namespace App\Controller;


use App\Logic\Calculate;
use App\Logic\ProjectDateCalculator;
use App\Structure\ProjectDate;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalculateDueDate
{

    /**
     * @Route("/calculate/{start}/{hour}", methods={"GET"})
     *
     * @param string $start
     * @param int $hour
     * @return JsonResponse
     */
    public function __invoke(string $start, int $hour)
    {
        $calculate = new Calculate(new ProjectDateCalculator());
        if(!$calculate->isInWorkTime($start)){
            return JsonResponse::create(['result' => 'This is a weekend day!'],Response::HTTP_BAD_REQUEST);
        }
        $projectDate = ProjectDate::crateProjectDate($start, $hour);

        return JsonResponse::create(['result' => $calculate->calculateDueDate($projectDate)],Response::HTTP_OK);
    }
}