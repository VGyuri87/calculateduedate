## Calculate due date

### Install composer
>
```sh 
composer install
```
##### Run docker
>
```sh
docker-compose up
```

#### Run functional tests
>
```sh
php ./vendor/bin/phpunit
```
