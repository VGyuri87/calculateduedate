<?php


namespace App\Tests;


use App\Logic\Calculate;
use App\Logic\ProjectDateCalculator;
use App\Structure\ProjectDate;
use PHPUnit\Framework\TestCase;

class CalcDueDateTest extends TestCase
{
    /**
     * @var Calculate
     */
    private $calculate;

    protected function setUp(): void
    {
        parent::setUp();
        $this->calculate = new Calculate(new ProjectDateCalculator());
    }


    public function weekendDataProvider(): array {
        return [
            [
                '2019-09-01',
                true,
            ],
            [
                '2019-09-02',
                false,
            ],
            [
                '2019-09-05',
                false,
            ],
            [
                '2019-09-07',
                true,
            ]
        ];
    }

    /**
     * @dataProvider weekendDataProvider
     * @param $date
     * @param $expected
     */
    public function testCheckIsWeekend($date, $expected){

        $this->assertEquals($this->calculate->isWeekend($date), $expected);
    }

    public function workTimeDataProvider(){
        return [
            [
                '2019-09-01 09:12:23',
                false,
            ],
            [
                '2019-09-01 08:12:23',
                false,
            ],
            [
                '2019-09-01 17:12:23',
                false,
            ],
            [
                '2019-09-03 09:12:23',
                true,
            ],
            [
                '2019-09-03 08:12:23',
                false,
            ],
            [
                '2019-09-03 16:59:23',
                true,
            ],
            [
                '2019-09-03 17:01:23',
                false,
            ],
        ];
    }

    /**
     * @dataProvider workTimeDataProvider
     * @param $date
     * @param $expected
     */
    public function testCheckIsInWorkTime($date, $expected){

        $this->assertEquals($this->calculate->isInWorkTime($date), $expected);
    }

    public function leftDayDataProvider(){
        return [
            [
                1,
                '0 day(s) and 1 hour(s)',
            ],
            [
                3,
                '0 day(s) and 3 hour(s)',
            ],
            [
                5,
                '0 day(s) and 5 hour(s)',
            ],
            [
                8,
                '1 day(s) and 0 hour(s)',
            ],
            [
                16,
                '2 day(s) and 0 hour(s)',
            ],
            [
                23,
                '2 day(s) and 7 hour(s)',
            ],
            [
                45,
                '5 day(s) and 5 hour(s)',
            ]
        ];
    }

    /**
     * @dataProvider leftDayDataProvider
     * @param $davalopTime
     * @param $expected
     */
    public function testDayAndHours($developTime, $expected){
        $this->assertEquals($this->calculate->getLeftDayAndHours($developTime), $expected);
    }

    public function calculateDatProvider(){
        return [
            [
                '2019-09-05 11:23:12',
                '0',
                '2019-09-05 11:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '1',
                '2019-09-05 12:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '3',
                '2019-09-05 14:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '5',
                '2019-09-05 16:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '6',
                '2019-09-06 09:23:00',
            ],
            [
                '2019-09-05 11:23:12',
                '7',
                '2019-09-06 10:23:00',
            ],
            [
                '2019-09-05 11:23:12',
                '8',
                '2019-09-06 11:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '16',
                '2019-09-09 11:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '24',
                '2019-09-10 11:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '36',
                '2019-09-11 15:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '48',
                '2019-09-13 11:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '66',
                '2019-09-17 13:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '128',
                '2019-09-27 11:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '120',
                '2019-09-26 11:23:12',
            ],
            [
                '2019-09-05 11:23:12',
                '125',
                '2019-09-26 16:23:12',
            ],
            [
                '2019-09-07 10:23:24',
                '23',
                'Today is weekend!'
            ]
        ];
    }

    /**
     * @dataProvider calculateDatProvider
     * @param $date
     * @param $developTime
     * @param $expectedDate
     */
    public function testCalculateDueDate($date, $developTime, $expectedDate){

        $projectDate = ProjectDate::crateProjectDate($date,$developTime);
        $this->assertEquals($this->calculate->calculateDueDate($projectDate),$expectedDate);
    }
}